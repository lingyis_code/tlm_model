#ifndef MEM_H
#define MEM_H


#include "tlm_utils/simple_initiator_socket.h"
#include "tlm_utils/simple_target_socket.h"
#include "tlm_utils/multi_passthrough_initiator_socket.h"
#include "tlm_utils/multi_passthrough_target_socket.h"
#include "tlm_utils/peq_with_cb_and_phase.h"

#include "ESL_sql_def.h"

#include <queue>
#include "tlm.h"

using namespace sc_core;
using namespace std;

#define DEBUG

//static ofstream fout("foo.txt");

// **************************************************************************************
// User-defined memory manager, which maintains a pool of transactions
// **************************************************************************************

class mm: public tlm::tlm_mm_interface
{
  typedef tlm::tlm_generic_payload gp_t;
#ifdef ESL_SQL
   SQL_GL_DEFINE
#endif

public:
  mm() : free_list(0), empties(0) {}

  gp_t* allocate();

#ifdef ESL_SQL
  gp_t* allocate(uint64 module_this_ptr_);
#endif


  void  free(gp_t* trans);

private:
  struct access
  {
    gp_t* trans;
    access* next;
    access* prev;
  };

  access* free_list;
  access* empties;

};

mm::gp_t* mm::allocate()
{
  gp_t* ptr;
  if (free_list)
  {
    ptr = free_list->trans;
    empties = free_list;
    free_list = free_list->next;
  }
  else
  {
    ptr = new gp_t(this);
  }
  return ptr;
}

#ifdef ESL_SQL
mm::gp_t* mm::allocate(uint64 module_this_ptr_)
{
  gp_t* ptr;
  if (free_list)
  {
    ptr = free_list->trans;
    empties = free_list;
    free_list = free_list->next;
  }
  else
  {
    ptr = new gp_t(this);
  }
 
  SQL_TID_ALLOCATE(ptr);
  SQL_INSERT_TRANCREATE(ptr, (int)module_this_ptr_, -1);
  return ptr;
}
#endif


void mm::free(gp_t* trans)
{
  if (!empties)
  {
    empties = new access;
    empties->next = free_list;
    empties->prev = 0;
    if (free_list)
      free_list->prev = empties;
  }
  free_list = empties;
  free_list->trans = trans;
  empties = free_list->prev;

#ifdef ESL_SQL
  SQL_TRANIDEXT_DESTROY(trans)
#endif

}

#ifdef ESL_SQL
  SQL_ID_INI
#endif

// Generate a random delay (with power-law distribution) to aid testing and stress the protocol
int rand_ps()
{
  int n = rand() % 100;
  n = n * n * n;
  return n / 100;
}



#endif
