#ifndef _ESL_SQL_H_
#define _ESL_SQL_H_

#define ESL_SQL

#ifdef ESL_SQL

#include "./tlm_extend/simple_initiator_socket_db.h"
#include "./tlm_extend/simple_target_socket_db.h"
#include "mysql4esl.h"

#define SQL_GL_DEFINE \
	static mysql_esl * glb_mysql_esl; \
	static uint64 m_txn_ID;

#define SQL_ID_INI \
    sc_dt::uint64 mm::m_txn_ID=0;

#define SQL_TID_ALLOCATE(x) \
	tranIdExtension * p_tran_id_ext = new tranIdExtension(m_txn_ID++); \
	x->set_extension(p_tran_id_ext);

#define SQL_INSERT_TRANCREATE(x, y, z) \
	glb_mysql_esl->global_mysql_esl()->esl_tran_create_table_insert(x, y, z)

#define SQL_TRANIDEXT_DESTROY(x) \
	tranIdExtension * p_tran_id_ext = 0; \
	x->get_extension<tranIdExtension>(p_tran_id_ext); \
	if(p_tran_id_ext==0){ \
	SC_REPORT_FATAL("Expected extension!", "This transaction does not have tranIdExtension!!!"); \
	} \
	else \
	{ \
	x->clear_extension(p_tran_id_ext); \
	delete p_tran_id_ext;}

#define SQL_MODULE_CREATE(x, y) \
	if((x)!=NULL) \
	{glb_mysql_esl->global_mysql_esl()->esl_module_info_table_insert((int)(x), 1, y);}

#define SQL_TRAN_RELEASE(x, y) \
	if(((x)!=NULL)&&(((x)->get_ref_count())==1)) \
	{mysql_esl glb_mysql_esl; glb_mysql_esl.global_mysql_esl()->esl_tran_destroy_table_insert((x), (int)y);}

#else
#endif


#endif
