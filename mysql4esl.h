#ifndef _MYSQL4ESL_H_
#define _MYSQL4ESL_H_


#include <iostream>
#include "tlm.h"
#include "systemc.h"
#include <time.h>
#include "mysql++.h"

typedef tlm::tlm_generic_payload  transaction_type;
typedef tlm::tlm_generic_payload * transaction_ptr_type;

class tranIdExtension: public tlm::tlm_extension <tranIdExtension>
{
public:
	tranIdExtension () : m_tran_id(0), in_database(false){};
	tranIdExtension (uint64 allocated_tran_id) :  m_tran_id (allocated_tran_id), in_database(false) {};
	virtual tlm_extension_base* clone() const
    {
		tranIdExtension* t =new tranIdExtension;
        t->m_tran_id = this->m_tran_id;
        return  t;
    };

	virtual void copy_from (tlm_extension_base const &ext)
    {
        m_tran_id = static_cast<tranIdExtension const&>(ext).m_tran_id;
    };
    ~tranIdExtension () {};
    uint64 m_tran_id;
    bool in_database;
};


class mysql_esl
{
public:
      mysql_esl (char * db_name, const char * user, const char * passwd)
      {
        conn.connect(0, "localhost", user, passwd);
        std::string someString = std::string (db_name);
        //const string &
        conn.create_db(someString.substr(0,strlen(db_name)-1));
        conn.connect((someString.substr(0,strlen(db_name)-1)).c_str(), "localhost", user, passwd);
        esl_tables_create();
       };
       mysql_esl(const char* db_name, const char * user)
       {
         conn.connect (0, "localhost", user);
         conn.create_db(db_name);
         conn.connect(db_name, "localhost" , user);
         esl_tables_create();
      };
       mysql_esl(const char* db_name)
        {
           conn.connect (0, "localhost");
           conn.create_db(db_name);
           conn.connect (db_name, "localhost");
           esl_tables_create();
       };

       mysql_esl ()
       {
       };

       static mysql_esl * global_mysql_esl ()
       {
         static mysql_esl * mysql_esl_instance;
         if(mysql_esl_instance==NULL)
         {
            time_t  rawtime;//=time (NULL);
            struct tm  * timeinfo;
            time (&rawtime);
            timeinfo= localtime(&rawtime);
            char * name = asctime (timeinfo);
            name[0] = 'E';
            name[1] = 'S';
            name[2]= 'L';
            name[3]= '_';
            for (int  i=4; name[i] != '\0' ;  i++)
            {
				if ((name[i] == ' ')||(name[i] == ':'))
                name [i] = '_';
			}
            mysql_esl_instance = new mysql_esl (name, "root", "llyict123");
          }
          return mysql_esl_instance;
       };

       void esl_tables_create()
       {
          char buf [500];
		  sprintf (buf, "CREATE TABLE module_info (ModuleID int (11), Having_socket int (11), Name varchar (100), PRIMARY KEY (ModuleID))");
          esl_table_query(buf);

          sprintf (buf, "CREATE TABLE module_hier_info (ModuleID int (11), SubModuleID int (11), Foreign KEY (ModuleID) references module_info(ModuleID))");
          esl_table_query(buf);

          sprintf (buf, "CREATE TABLE socket_info (SocketID int (11),Socket_Name varchar (100), Is_initiator int (11), Creator_ModuleID int (11), PRIMARY KEY (SocketID), Foreign KEY  (Creator_moduleID) references module_info (ModuleID)) ");
          esl_table_query(buf);

          sprintf (buf, "CREATE TABLE transaction_create (TransactionID int (11), Creator_ModuleID int (11), Time double, Tsize int (11), PRIMARY KEY (TransactionID))");
          esl_table_query(buf); 

		  sprintf (buf, "CREATE TABLE transaction_info(TransactionID int (11), Taddress  int (11), Tcommand varchar (100), Tlength int (11), Foreign KEY (TransactionID) references transaction_create (TransactionID) ) ");
          esl_table_query (buf);

          sprintf (buf, "CREATE TABLE transaction_pass_skt (TransactionID int (11), SocketID int (11), Time double, phase varchar (100), RESP_status varchar (100), Ref_count int (11), Is_Forward int (11), Foreign Key (TransactionID) references transaction_create (TransactionID) ) ");
          esl_table_query(buf);

          sprintf (buf, "CREATE TABLE fifo_create (FifoID int (11),Name varchar(100), Size int (11), Creator_ModuleID int (11), Time double,  PRIMARY KEY (FifoID), Foreign KEY (Creator_ModuleID) references module_info(ModuleID))"); 
		  esl_table_query(buf);

          sprintf (buf, "CREATE TABLE transaction_pass_fifo (TransactionID int (11), FifoID int (11), Time double,  Is_write0p int (11), Foreign Key (TransactionID) references transaction_create (TransactionID), Foreign Key (FifoID) references fifo_create(FifoID) ) ");
          esl_table_query(buf);

          sprintf (buf, "CREATE TABLE transaction_destroy (TransactionID int (11), Time double, Destroyer_ModuleID   int (11), Foreign Key (TransactionID) references transaction_create (TransactionID), Foreign Key (Destroyer_ModuleID) references module_info (ModuleID) )");
          esl_table_query(buf);

          //  sprintf (buf, “ CREATE TABLE tran_map (TID1 int (11), TID2 int (11), Time double, MID int (11) ) ” );
          // esl_table_query(buf);
          // sprintf (buf, “ CREATE TABLE link_spec (LID1 int (11), src_name varchar(100),dst_name varchar (100), latency_spec double, thruput_spec double) ”);
          // esl_table_query(buf);
        };
        void esl_table_query (const char * buf)
        {
          mysqlpp::Query query = conn.query();
          // std: : cout <<buf <<std: : endl;
          //-----query << buf;
          //-----mysqlpp::SimpleResult res = query.execute();
        };
		/////
        void esl_socket_info_table_insert (int socket_id, const char * socket_name, int ini_tgt, int module_id)
        {
          char buf [500];
          // socket _info (SocketID int (11), Socket_Name varchar(100), Is_initiator int (11), Creator_ModuleID int (11)
          sprintf (buf, "INSERT INTO socket_info VALUES (%d,'%s', %d, %d); \n", socket_id, socket_name, ini_tgt, module_id);
          esl_table_query(buf);
        };

        void esl_FIFO_table_insert (const char * fifo_name, int fifo_id, int size,int module_id)
        {
          char buf [500];
          // (FifoID int (11), Name varchar (100), Size int (11), Creator _ModuleID int(11)
          sprintf (buf, "INSERT INTO fifo_create VALUES (%d,'%s', %d, %d); ", fifo_id, fifo_name, size, module_id);
          esl_table_query(buf);
        };
        void esl_tran_pass_skt_insert (transaction_type&trans, int socket_id, int phase, int fw_bw)
        {
          tranIdExtension* p_mysrcid;
          p_mysrcid=trans.get_extension< tranIdExtension >();
          char buf [500];
          double cur_time = sc_core::sc_time_stamp().to_seconds()*1e9;
          char phase_string [100];
          switch  (phase)
          {
            case 1:	
              strcpy (phase_string, "BEGIN_REQ");
              break;
            case  2:
              strcpy (phase_string, "END_REQ");
              break;
            case 3:	
              strcpy (phase_string, "BEGIN_RESP");
              break;
            case 4:	
              strcpy (phase_string, "END_RESP");
              break;
            default:	
              strcpy (phase_string, "UNKNOWN");
              break;
          }
          //switch (trans.get_response_string()  )
          std::string resp_string = trans.get_response_string();
          int ref_cnt = trans.get_ref_count();
		  //cout<<p_mysrcid->m_tran_id<<endl;
          //TransactionID int (11), SocketID int (11), Time double, phase varchar (100), RESP_status varchar (100), Ref_count  int (11), Is_Forward int  (11),
          sprintf(buf, "INSERT INTO transaction_pass_skt VALUES (%llu,%d, %10f, '%s', '%s', %d, %d); ", (p_mysrcid->m_tran_id), socket_id, (double) cur_time, phase_string, resp_string.c_str(), ref_cnt, fw_bw);
          esl_table_query(buf);
          //esl_tran_info_insert(trans);
        };

       void esl_tran_info_insert(transaction_type&trans)
       {
          tranIdExtension* p_mysrcid;
          p_mysrcid =trans.get_extension< tranIdExtension>();
          bool in_database = p_mysrcid->in_database;
          char cmd_string[100];
		  if(in_database == false)
          {
            switch  (trans.get_command())
            {
              case tlm::TLM_READ_COMMAND:
                strcpy(cmd_string, "TLM_READ_COMMAND");
                break;
              case tlm::TLM_WRITE_COMMAND:
                strcpy(cmd_string, "TLM_WRITE_COMMAND");
                break;
              case tlm::TLM_IGNORE_COMMAND:
                strcpy(cmd_string, "TLM_IGNORE_COMMAND");
                break;
              default : 
                strcpy (cmd_string, "UNKNOWN");
                break;
            }
              //  transaction_info(TransactionID int (11), Taddress int (11), Tcommand varchar(100), Tlength int(11),
              char buf[500];
              sprintf(buf, "INSERT INTO transaction_info  VALUES (%llu,%llu, '%s', %d); ", (p_mysrcid->m_tran_id), trans.get_address(), cmd_string, trans.get_data_length());
              esl_table_query(buf);
              p_mysrcid->in_database =true;
          }
      }
      void esl_tran_pass_fifo_insert (transaction_ptr_type trans_ptr, int FIFO_id, int in_out)
      {
          tranIdExtension* p_mysrcid;
          p_mysrcid = trans_ptr->get_extension< tranIdExtension>();
		  if(p_mysrcid != NULL)
          {
            char buf[500];
            double cur_time = sc_core::sc_time_stamp().to_seconds()*1e9;
            //(TransactionID int (11), FifoID int (11), Time double, Is_weite0p int (11),
            sprintf(buf, "INSERT INTO transaction_pass_fifo VALUES (%llu,%d,%10f, %d); ", (p_mysrcid->m_tran_id), FIFO_id, (double) cur_time, in_out);
            esl_table_query(buf);
          }
          else
          {
            std::cout << "Transaction is not allocated by TLM memory manager!" << std::endl;
            char buf[500];
            double cur_time = sc_core::sc_time_stamp ().to_seconds()*1e9;
            sprintf(buf, "INSERT INTO transaction_pass_fifo VALUES (%d,%d,%10f, %d); ", (-1), FIFO_id, (double) cur_time, in_out); 
       		esl_table_query(buf);
          }
	  }
      void esl_tran_create_table_insert (transaction_ptr_type  tran_ptr, int module_id, int tsize)
      {
        tranIdExtension* p_mysrcid;
        p_mysrcid =tran_ptr->get_extension< tranIdExtension> ();
        char buf[500];
        double cur_time = sc_core::sc_time_stamp().to_seconds()*1e9;
        //(TransactionID int (11), Creator_ModuleID  int (11), Time double, Tsize int (11),
        sprintf(buf, "INSERT INTO transaction_create VALUES (%llu,%d,%10f, %d); ", (p_mysrcid->m_tran_id), module_id, (double) cur_time, tsize);
        esl_table_query(buf);
      };
      
	  void esl_tran_destroy_table_insert (transaction_ptr_type tran_ptr, int module_ID)
      {
        tranIdExtension* p_mysrcid;
        p_mysrcid =tran_ptr->get_extension< tranIdExtension> () ;
        char buf[500];
        double cur_time = sc_core::sc_time_stamp().to_seconds()*1e9;
        //(TransactionID int (11), Time double, Destroyer_ModuleID int(11),
        sprintf(buf, "INSERT INTO transaction_destroy VALUES (%llu,%10f, %d);\n ", p_mysrcid->m_tran_id, cur_time,module_ID); 
		esl_table_query(buf);
       };

      void esl_module_info_table_insert (int module_ID, int having_socket, const char * mname)
      {
        char buf[500];
        //ModuleID int (11), Having_socket int (11), Name varchar (100),
        sprintf(buf, "INSERT INTO module_info  VALUES (%d, %d,'%s'); ",  module_ID, having_socket, mname);
        esl_table_query(buf);
      };
      void esl_module_hier_info_table_insert (int module_ID, int  submodule_ID)
      {
        //Module_hier_info   (ModuleID int (11), SubModuleID int(11), Name varchar(100)
        char buf[500];
        sprintf(buf, "INSERT INTO module_hier_info  VALUES (%d, %d); ", module_ID,submodule_ID);
        esl_table_query(buf);
      }
private:
      mysqlpp::Connection conn;
};


#endif
