
#define SC_INCLUDE_DYNAMIC_PROCESSES

#include "tlm_utils/simple_initiator_socket.h"
#include "tlm_utils/simple_target_socket.h"
#include "tlm_utils/multi_passthrough_initiator_socket.h"
#include "tlm_utils/multi_passthrough_target_socket.h"
#include "tlm_utils/peq_with_cb_and_phase.h"
#include <queue>
#include <list>
#include <stdlib.h>

#include "mem.h"


extern ofstream fout;

using namespace sc_core;
using namespace std;
using namespace tlm_utils;

#define DEBUG

//#define PRIORITY

//static ofstream fout("foo.txt");

// ************************************************************************************
// Bus model supports multiple initiators and multiple targets
// Supports b_ and nb_ transport interfaces, DMI and debug
// It does no arbitration, but routes all transactions from initiators without blocking
// It uses a simple built-in routing algorithm
// ************************************************************************************

template<unsigned int BUSWIDTH>
class FW_block: public sc_core::sc_module
{
  // ***********************************************************
  // Each multi-socket can be bound to multiple sockets
  // No need for an array-of-sockets
  // ***********************************************************
	
public:
	SC_HAS_PROCESS(FW_block);

  //tlm_utils::simple_target_socket_tagged<FW_block, BUSWIDTH>    targ_socket;
  //tlm_utils::simple_initiator_socket_tagged<FW_block, BUSWIDTH> init_socket;
#ifdef ESL_SQL
  tlm_utils::simple_target_socket_db<FW_block>    targ_socket;
  tlm_utils::simple_initiator_socket_db<FW_block> init_socket;
#else
  tlm_utils::simple_target_socket<FW_block>    targ_socket;
  tlm_utils::simple_initiator_socket<FW_block> init_socket;
#endif
  //SC_CTOR(Bus)
  //: targ_socket("targ_socket"), init_socket("init_socket")
  FW_block(sc_module_name module_name)
	  :sc_core::sc_module(module_name)
  {
	  targ_socket.register_nb_transport_fw(   this, &FW_block::nb_transport_fw);
	  init_socket.register_nb_transport_bw(          this, &FW_block::nb_transport_bw);
	 
	  SC_THREAD(thread_nb_write);
	  SC_THREAD(thread_nb_read);
	  //initialize();
  }

  void thread_nb_write()
  {
	  while(1)
	  {

		  wait(SC_ZERO_TIME);
		  /*sc_dt::uint64 delta_cycle = 0;
		  if(!sc_pending_activity_at_current_time())
			  delta_cycle = sc_delta_count();*/     // COMMENTME

			  if(write_trans.size()>0)
			  {
			    typename std::list<trans_in_flight *>::iterator it;
		            for(it=write_trans.begin();it!=write_trans.end();it++)
				{
					if((*it)->in_flight==false)
					{
						//send out the transaction
						// tlm::tlm_sync_enum status = init_socket->nb_transport_fw(*((*it)->Ptrans), (*it)->phase, (*it)->delay);      // COMMENTME
						(*it)->in_flight=true;
						break;
					}
				} 
			  }
		  wait(1, SC_NS);
	  }
  }

  void thread_nb_read()
  {
	  while(1)
	  {
		  wait(SC_ZERO_TIME);
		  /*sc_dt::uint64 delta_cycle = 0;
		  if(!sc_pending_activity_at_current_time())
			  delta_cycle = sc_delta_count();*/     // COMMENTME

			  if(read_trans.size()>0)
			  {
			    typename std::list<trans_in_flight *>::iterator it;
		            for(it=read_trans.begin();it!=read_trans.end();it++)
				{
					if((*it)->in_flight==false)
					{
						//send out the transaction
						// tlm::tlm_sync_enum status = init_socket->nb_transport_fw(*((*it)->Ptrans), (*it)->phase, (*it)->delay);      // COMMENTME
						(*it)->in_flight=true;
						break;
					}
				} 
			  }
		  
		  wait(1, SC_NS);
	  }
  }



  // Tagged non-blocking transport forward method
  // ##############################################
  virtual tlm::tlm_sync_enum nb_transport_fw(//int id,
      tlm::tlm_generic_payload& trans, tlm::tlm_phase& phase, sc_time& delay)
  {
    //assert (id < targ_socket.size());
	 // cout<<"In FW blcok: FW "<<phase<<std::endl;
    // Forward path
    //m_id_map[ &trans ] = id;

    sc_dt::uint64 address = trans.get_address();
    sc_dt::uint64 masked_address = address;
    unsigned int target_nr = 0;

    if (1)
    {
      // Modify address within transaction
      trans.set_address( masked_address );

	  
      // Forward transaction to appropriate target
	  if(phase==tlm::END_RESP)
	  {
            tranIdExtension* p_mysrcid = trans.get_extension< tranIdExtension>();
	    fout<<"Tran "<<p_mysrcid->m_tran_id<<" : Fun: Dequeue"<<" Module: "<<this->name()<<" Time: "<<sc_time_stamp()<<endl;

	    remove_trans(trans, phase, delay, target_nr);
		tlm::tlm_sync_enum status = init_socket->nb_transport_fw(trans, phase, delay);
		return status;
	  }
	  else
	  {
		  //tlm::BEGINE_REQ
		  tranIdExtension* p_mysrcid = trans.get_extension< tranIdExtension>();
	          fout<<"Tran "<<p_mysrcid->m_tran_id<<" : Fun: Enqueue"<<" Module: "<<this->name()<<" Time: "<<sc_time_stamp()<<endl;
		  
		  insert_trans(trans, phase, delay, 0, target_nr);
		  phase = tlm::END_REQ;
		  return tlm::TLM_UPDATED;
	  }
    }
    else
      return tlm::TLM_COMPLETED;
  }

  // Tagged non-blocking transport backward method
  // ##############################################
  virtual tlm::tlm_sync_enum nb_transport_bw(//int id,
      tlm::tlm_generic_payload& trans, tlm::tlm_phase& phase, sc_time& delay)
  {
    sc_dt::uint64 address = trans.get_address();
    trans.set_address(address);
	if(phase == tlm::END_REQ)
		return tlm::TLM_ACCEPTED;
    return targ_socket->nb_transport_bw(trans, phase, delay);
  }

  
  void remove_trans(tlm::tlm_generic_payload & trans, tlm::tlm_phase & phase, sc_time & delay, unsigned int target_nr)
  {
	bool flag=false;
        sc_dt::uint64 m_tran_id = trans.get_extension< tranIdExtension>()->m_tran_id;
	//cout<<"Current Transaction ID: "<<m_tran_id<<endl;
	if(trans.is_write())
	{
		typename std::list<trans_in_flight *>::iterator it;
		for(it=write_trans.begin();it!=write_trans.end();it++)
		{
                        sc_dt::uint64 it_tran_id = (*it)->Ptrans->template get_extension<tranIdExtension>()->m_tran_id;
			//cout<<"WR: In queue ID: "<<it_tran_id<<endl;

			if(m_tran_id==it_tran_id)
			{
				delete (*it);
				write_trans.erase(it);
				flag=true;
				break;
			}
		}
		if(flag==false)
			std::cerr<<"Fatal error, not able to find the write transaction"<<std::endl;
	}
	else if(trans.is_read())
	{
		
		typename std::list<trans_in_flight *>::iterator it;
		for(it=read_trans.begin();it!=read_trans.end();it++)
		{
			
                        sc_dt::uint64 it_tran_id = (*it)->Ptrans->template get_extension<tranIdExtension>()->m_tran_id;
			//if((*it)->m_tran_id == m_tran_id)
			//cout<<"Rd In queue ID: "<<it_tran_id<<endl;
			
			if(m_tran_id==it_tran_id)
			{
				delete (*it);
				read_trans.erase(it);
				flag=true;
				break;
			}
		}
		if(flag==false)
			std::cerr<<"Fatal error, not able to find the read transaction"<<std::endl;
	}
  }
  void insert_trans(tlm::tlm_generic_payload & trans, tlm::tlm_phase & phase, sc_time & delay, unsigned int init_nr, unsigned int target_nr)
  {
	trans_in_flight * cur_trans=new trans_in_flight;
	cur_trans->Ptrans= &trans;
	cur_trans->phase = phase;
	cur_trans->delay = delay;

	//Push into FIFO
	if(trans.is_write())
	{
		write_trans.push_back(cur_trans);
	}
	else if(trans.is_read())
	{
		read_trans.push_back(cur_trans);
	}

	cout<<"Depth of the WR Trans fifo in Module "<<this->name()<<" is "<<write_trans.size()<<endl;
	cout<<"Depth of the RD Trans fifo in Module "<<this->name()<<" is "<<read_trans.size()<<endl;
  }

  struct trans_in_flight
  {
	  trans_in_flight():Ptrans(NULL), phase(tlm::TLM_ACCEPTED), delay(SC_ZERO_TIME), 
	  in_flight(false)
	  {
	  }
	  tlm::tlm_generic_payload * Ptrans;
	  //tranIdExtension* p_mysrcid = trans.get_extension< tranIdExtension>();
	  //UINT64 m_tran_id;
	  tlm::tlm_phase phase;
	  sc_time delay;
	  bool in_flight;
  };


  std::list<trans_in_flight *> read_trans;
  std::list<trans_in_flight *> write_trans; 

};



