#ifndef __L2MEMORY_H__
#define __L2MEMORY_H__

#include "tlm_utils/simple_initiator_socket.h"
#include "tlm_utils/simple_target_socket.h"
#include "tlm_utils/multi_passthrough_initiator_socket.h"
#include "tlm_utils/multi_passthrough_target_socket.h"
#include "tlm_utils/peq_with_cb_and_phase.h"

#include <list>
#include "mem.h"

#include "./tlm_extend/simple_target_socket_db.h"

//#include <boost/numeric/ublas/storage_sparse.hpp>


#include <queue>
#include <map>

using namespace sc_core;
using namespace std;
using namespace sc_dt;
//using namespace boost::numeric::ublas;

#define DEBUG

extern ofstream fout;

// Generate a random delay (with power-law distribution) to aid testing and stress the protocol
int delay_ps()
{
//    int n = rand() % 100;
//    n = n * n * n;
//    return n / 100;
    return 1;
}


// **************************************************************************************
// L2Memory module able to handle
// **************************************************************************************

DECLARE_EXTENDED_PHASE(internal_phase);


// NOTE: a clock of 1GHz is assumed (one cycle is 1 ns)
template<unsigned int BUSWIDTH>
class L2Memory: public sc_module
{
public:
  // TLM-2 socket, defaults to 32-bits wide, base protocol
//#ifdef ESL_ASSERT
//    tlm_utils::simple_target_socket_assert<L2Memory, BUSWIDTH> socket;
//#elif defined(ESL_SQL)
//    tlm_utils::simple_target_socket_sql<L2Memory, BUSWIDTH> socket;
//#else
//    tlm_utils::simple_target_socket<L2Memory, BUSWIDTH> socket;
//#endif

#ifdef ESL_SQL
  tlm_utils::simple_target_socket_db<L2Memory> socket;
#else
  tlm_utils::simple_target_socket_db<L2Memory> socket;
#endif


    //SC_CTOR(L2Memory)
    L2Memory(sc_core::sc_module_name name_)
        : sc_module(name_)
        , socket("socket")
        , m_peq(this, &L2Memory::peq_cb)
        , m_bus_width_in_bytes(BUSWIDTH >> 3)
        , mp_fout(0)
    {
      // Register callbacks for incoming interface method calls
        socket.register_nb_transport_fw(this, &L2Memory::nb_transport_fw);
        socket.register_b_transport    (this, &L2Memory::b_transport);
        socket.register_transport_dbg  (this, &L2Memory::transport_dbg);

        for (unsigned int i = 0; i < NR_IO_OP; i++)
        {
            m_nr_requests_pipelined[i] = 0;
            m_is_response_in_progress[i] = false;
            //mp_next_response_pending[i] = 0;
            //mp_end_req_pending[i] = 0;
            m_interleave_depth[i] = 8; // FIXME
        }
        m_start_addr_physical_bank[0] = 0;
        for (unsigned int i = 1; i < NR_PHYSICAL_MEM_BANKS; i++)
        {
            m_start_addr_physical_bank[i] = m_start_addr_physical_bank[i - 1] + PHYSICAL_BANK_WORD_SIZE_IN_BYTES;
        }
        SC_HAS_PROCESS(L2Memory);
        SC_THREAD(thread_manage_phys_banks);
    }


//    void set_log( ofstream* p_fout_)
//    {
//        mp_fout = p_fout_;
//    }


    virtual void b_transport( tlm::tlm_generic_payload& txn_, sc_time& delay_ )
    {
        assert(0 && "FIXME! To be implemented!");
#ifdef DEBUG
          fout << "txn = " << &txn_ << " addr = " << txn_.get_address() << " " << name() << " b_transport at " << sc_time_stamp() << endl;
#endif
        wait(10, SC_PS);
        txn_.set_response_status( tlm::TLM_OK_RESPONSE );
    }


    virtual unsigned int transport_dbg( tlm::tlm_generic_payload& txn_)
    {
        assert(0 && "FIXME! To be implemented!");
#ifdef DEBUG
          fout << "txn = " << &txn_ << "addr = " << txn_.get_address() << " " << name() << " transport_dbg at " << sc_time_stamp() << endl;
#endif

        txn_.set_response_status( tlm::TLM_OK_RESPONSE );
        return txn_.get_data_length(); // FIXME: return the number of bytes read/write
    }


    virtual tlm::tlm_sync_enum nb_transport_fw( tlm::tlm_generic_payload& txn_,
                                                tlm::tlm_phase& phase_, sc_time& delay_)
    {
        if (txn_.get_byte_enable_ptr() != 0) {
            txn_.set_response_status( tlm::TLM_BYTE_ENABLE_ERROR_RESPONSE );
            return tlm::TLM_COMPLETED;
        }
        // queue the transaction until the annotated time has elapsed
        m_peq.notify( txn_, phase_, delay_);

	    // The following code is added by HuangChushun, but no effect. 
/*
        if( phase_ == tlm::END_RESP ) 
        { 
            return tlm::TLM_COMPLETED; 
        } 
*/
        return tlm::TLM_ACCEPTED;
    }
private:
    void peq_cb(tlm::tlm_generic_payload& txn_, const tlm::tlm_phase& phase_)
    {
        switch (phase_)
        {
        case tlm::BEGIN_REQ:
#ifdef DEBUG
            fout << "txn = " << &txn_ << " addr = " << txn_.get_address() << " " << name() << " BEGIN_REQ at " << sc_time_stamp() << endl;
#endif
            txn_.acquire();
            // Put back-pressure on initiator by deferring END_REQ until pipeline is clear
            if ((m_nr_requests_pipelined[txn_.is_write()] == m_interleave_depth[txn_.is_write()]) ||
                is_physical_bank_contention(txn_))
            {
                //##assert(mp_end_req_pending[txn_.is_write()] == 0);
                //##mp_end_req_pending[txn_.is_write()] = &txn_;
				mp_end_req_pending[txn_.is_write()].push(&txn_);
            }
            else
            {
                tlm::tlm_sync_enum status = send_end_req(txn_);
                if (status == tlm::TLM_COMPLETED) // It is questionable whether this is valid
                     SC_REPORT_FATAL("ERROR", "Not expected status TLM_COMPLETED received!");
            }
        break;
        case tlm::END_RESP:
            // On receiving END_RESP, the target can release the transaction
            // and allow other pending transactions to proceed
#ifdef DEBUG
              fout << "txn = " << &txn_ << " addr = " << txn_.get_address() << " " << name() << " END_RESP at " << sc_time_stamp() << endl;
#endif

            if (!m_is_response_in_progress[txn_.is_write()])
                SC_REPORT_FATAL("TLM-2", "Illegal transaction phase END_RESP received by target");
#ifdef ESL_SQL
            SQL_TRAN_RELEASE(&txn_,(long)this)
#endif
         //rcm   txn_.release();
            m_nr_requests_pipelined[txn_.is_write()]--;

            // L2Memory itself is now clear to issue the next BEGIN_RESP
            m_is_response_in_progress[txn_.is_write()] = false;
            //#if (mp_next_response_pending[txn_.is_write()])
			if(!mp_next_response_pending[txn_.is_write()].empty())
            {
                //##send_response( *mp_next_response_pending[txn_.is_write()] );
				send_response( *(mp_next_response_pending[txn_.is_write()].front()));
                //###
				mp_next_response_pending[txn_.is_write()].pop();;
            }

            // ... and to unblock the initiator by issuing END_REQ
            //#if (mp_end_req_pending[txn_.is_write()])
			if (!mp_end_req_pending[txn_.is_write()].empty())
            {
                //##if (!is_physical_bank_contention(*mp_end_req_pending[txn_.is_write()]))
				if (!is_physical_bank_contention(*(mp_end_req_pending[txn_.is_write()].front())))
                {
                    //##tlm::tlm_sync_enum status = send_end_req( *mp_end_req_pending[txn_.is_write()] );
					// tlm::tlm_sync_enum status = send_end_req( *(mp_end_req_pending[txn_.is_write()].front()));       // COMMENTME
                    //##mp_end_req_pending[txn_.is_write()] = 0;
                    mp_end_req_pending[txn_.is_write()].pop();  
                }
            }
			txn_.release();

        break;
        case tlm::END_REQ:
        case tlm::BEGIN_RESP:
            SC_REPORT_FATAL("TLM-2", "Illegal transaction phase received by target");
        break;
        default:
            if (phase_ == internal_phase)
            {
                // Execute the read or write commands
                tlm::tlm_command cmd = txn_.get_command();
                sc_dt::uint64    adr = txn_.get_address();
                unsigned char*   data_ptr = txn_.get_data_ptr();

                if ( cmd == tlm::TLM_READ_COMMAND )
                {
                    *reinterpret_cast<int*>(data_ptr) = rand();
#ifdef DEBUG
                    fout << "txn = " << &txn_ << " addr = " << adr << " " << name() << " End execute READ, target = " << name()
                         << " data = " << *reinterpret_cast<int*>(data_ptr) << " at " << sc_time_stamp() << endl;
#endif
                }
                else if ( cmd == tlm::TLM_WRITE_COMMAND )
                {
#ifdef DEBUG
                    fout << "txn = " << &txn_ << " addr = " << adr << " " << name() << " End execute WRITE, target = " << name()
                         << " data = " << *reinterpret_cast<int*>(data_ptr) << " at " << sc_time_stamp() << endl;
#endif
                }

                txn_.set_response_status( tlm::TLM_OK_RESPONSE );

                // L2Memory must honor BEGIN_RESP/END_RESP exclusion rule
                // i.e. must not send BEGIN_RESP until receiving previous END_RESP or BEGIN_REQ
                if (m_is_response_in_progress[txn_.is_write()])
                {
                  // L2Memory allows only two transactions in-flight
					//##if (mp_next_response_pending[txn_.is_write()]) {
                    //##    SC_REPORT_FATAL("TLM-2", "Attempt to have two pending responses in target!");
					//##}

                    //##mp_next_response_pending[txn_.is_write()] = &txn_;
					mp_next_response_pending[txn_.is_write()].push(&txn_);
                }
                else
                {
                    send_response(txn_);
                }
                break;
            }
        }
    }

    tlm::tlm_sync_enum send_end_req(tlm::tlm_generic_payload& txn_)
    {
        // Queue the acceptance and the response with the appropriate latency
        tlm::tlm_phase end_req_phase = tlm::END_REQ;
        sc_time req_accept_delay = sc_time(delay_ps(), SC_PS); // Accept delay
        tlm::tlm_sync_enum status = socket->nb_transport_bw( txn_, end_req_phase, req_accept_delay );
        assert(status == tlm::TLM_ACCEPTED);

        // Queue internal event to mark beginning of response
        tlm::tlm_command cmd = txn_.get_command();
        sc_dt::uint64    adr = txn_.get_address();
        unsigned char*   data_ptr = txn_.get_data_ptr();

        if ( cmd == tlm::TLM_READ_COMMAND )
        {
            *reinterpret_cast<int*>(data_ptr) = rand();
#ifdef DEBUG
            fout << "txn = " << &txn_ << " addr = " << adr << " " << name() << " Start execute READ, target = " << name()
                       << " data = " << *reinterpret_cast<int*>(data_ptr) << " at " << sc_time_stamp() << endl;
#endif
        }
        else if ( cmd == tlm::TLM_WRITE_COMMAND )
        {
#ifdef DEBUG
                fout << "txn = " << &txn_ << " addr = " << adr << " " << name() << " Start execute WRITE, target = " << name()
                           << " data = " << *reinterpret_cast<int*>(data_ptr) << " at " << sc_time_stamp() << endl;
#endif
        }

        unsigned int nr_cycles_per_txn = compute_nr_phys_banks_used(txn_); // one physical bank word is written per cycle

        sc_time processing_time = sc_time(nr_cycles_per_txn, SC_NS); // FIXME this assuems 1 GHz clock
        m_peq.notify( txn_, internal_phase, processing_time);
        m_nr_requests_pipelined[txn_.is_write()]++;
        populate_physical_bank_per_cycle(txn_);
        return status;
    }

    void send_response(tlm::tlm_generic_payload& txn_)
    {
        if(txn_.is_write()) {
            write(txn_);
        }
        else {
            read(txn_);
        }

        m_is_response_in_progress[txn_.is_write()] = true;
        tlm::tlm_phase bw_phase = tlm::BEGIN_RESP;
        sc_time delay = SC_ZERO_TIME;
        tlm::tlm_sync_enum status = socket->nb_transport_bw( txn_, bw_phase, delay );

        if (status == tlm::TLM_UPDATED)
        {
            // The timing annotation must be honored
            m_peq.notify( txn_, bw_phase, delay);
        }
        else if (status == tlm::TLM_COMPLETED)
        {
            // The initiator has terminated the transaction
#ifdef ESL_SQL
            SQL_TRAN_RELEASE(&txn_, (long)this)
#endif
//            remove_txn_in_flight(txn_);
            txn_.release();
            m_nr_requests_pipelined[txn_.is_write()]--;
            m_is_response_in_progress[txn_.is_write()] = false;
        }
    }

    void read(tlm::tlm_generic_payload& txn_)
    {
        sc_dt::uint64    addr          = txn_.get_address();
        unsigned char*   data_ptr      = txn_.get_data_ptr();

        printf("READ (addr, data)\n");
        for (unsigned int i = 0; i < txn_.get_data_length(); i++)
        {
            *(data_ptr + i) = m_storage[addr + i];
            printf(" (0x%llu, 0x%x)", addr + i, (unsigned int)m_storage[addr + i]);     // COMMENTME
        }
        printf("\nEND READ\n");

    }

    void write(tlm::tlm_generic_payload& txn_)
    {
        sc_dt::uint64    addr          = txn_.get_address();
        unsigned char*   data_ptr      = txn_.get_data_ptr();

        printf("WRITE (addr, data)\n");
        for (unsigned int i = 0; i < txn_.get_data_length(); i++)
        {
            m_storage[addr + i] = *(data_ptr + i);
            printf(" (0x%llu, 0x%x)", addr + i, (unsigned int)m_storage[addr + i]);     // COMMENTME
        }
        printf("\nEND WRITE\n");

    }

    bool is_physical_bank_contention(tlm::tlm_generic_payload& txn_)
    {
        unsigned int first_bank = find_first_bank (txn_);
        unsigned int bank_per_first_cycle = 1 << first_bank;
		
		if (m_phys_banks_per_cycle.size() == 0)
		{
			m_phys_banks_per_cycle.push_back(0);
		}
		assert(m_phys_banks_per_cycle.size() > 0);
        return bank_per_first_cycle & m_phys_banks_per_cycle[0].to_uint(); // m_phys_banks_per_cycle[0] represents the physical bank usage for the current cycle
    }

    unsigned int find_first_bank (tlm::tlm_generic_payload& txn_)
    {
        unsigned int ret = NR_PHYSICAL_MEM_BANKS;
        for (unsigned int i = 0; i < NR_PHYSICAL_MEM_BANKS - 1; i++)
        {
            if (((txn_.get_address() & 0xff) >= m_start_addr_physical_bank[i]) &&
                ((txn_.get_address() & 0xff) < m_start_addr_physical_bank[i + 1]))
            {
                ret = i;
                break;
            }
        }
        //###assert(ret < NR_PHYSICAL_MEM_BANKS);
		if(ret == NR_PHYSICAL_MEM_BANKS) return ret-1;
        return ret;
    }

    unsigned int compute_nr_phys_banks_used(tlm::tlm_generic_payload& txn_)
    {
        sc_dt::uint64 aligned_addr = txn_.get_address() & ADDR_MASK_PHYSICAL_BANK;
        unsigned int data_gap = txn_.get_address() - aligned_addr;
        unsigned int ret = (data_gap + txn_.get_data_length() + PHYSICAL_BANK_WORD_SIZE_IN_BYTES - 1) / PHYSICAL_BANK_WORD_SIZE_IN_BYTES;
        return ret;
    }

    void populate_physical_bank_per_cycle(tlm::tlm_generic_payload& txn_)
    {
         unsigned int nr_phys_banks_used_by_txn = compute_nr_phys_banks_used(txn_);
        if (m_phys_banks_per_cycle.size() < nr_phys_banks_used_by_txn)
        {
            unsigned int extra_bank_needed_for_current_txn = nr_phys_banks_used_by_txn - m_phys_banks_per_cycle.size();
            for (unsigned int i = 0; i < extra_bank_needed_for_current_txn; i++)
            {
                m_phys_banks_per_cycle.push_back(0);
            }
            unsigned int first_bank = find_first_bank (txn_);
            for (unsigned int i = 0; i < nr_phys_banks_used_by_txn; i++)
            {
                m_phys_banks_per_cycle[i] = m_phys_banks_per_cycle[i] | (sc_uint<NR_PHYSICAL_MEM_BANKS>)(1 << ((first_bank + i)%NR_PHYSICAL_MEM_BANKS)) ;
            }
        }

    }

    void thread_manage_phys_banks()
    {
        while(1)
        {
            wait(1, SC_NS); // FIXME this assumes 1GHz clock
            if (m_phys_banks_per_cycle.size() > 0)
            {
                m_phys_banks_per_cycle.pop_front();
                cout << "m_phys_banks_per_cycle.size() = " << m_phys_banks_per_cycle.size() << endl;
            }
        }
    }

    enum IO_OP
    {
        READ_OP = 0,
        WRITE_OP,
        NR_IO_OP
    };

    static const unsigned int NR_PHYSICAL_MEM_BANKS = 8;
    static const sc_dt::uint64 ADDR_MASK_PHYSICAL_BANK = 0xffffffffffffffe0;
    static const unsigned int PHYSICAL_BANK_WORD_SIZE_IN_BYTES = 0x20;
    deque<sc_uint<NR_PHYSICAL_MEM_BANKS> > m_phys_banks_per_cycle;
    unsigned int m_start_addr_physical_bank[NR_PHYSICAL_MEM_BANKS];
    unsigned int   m_nr_requests_pipelined[NR_IO_OP];
    bool  m_is_response_in_progress[NR_IO_OP];
    //tlm::tlm_generic_payload*  mp_next_response_pending[NR_IO_OP];
    //tlm::tlm_generic_payload*  mp_end_req_pending[NR_IO_OP];

	//support waiting list
    queue<tlm::tlm_generic_payload*>  mp_next_response_pending[NR_IO_OP];
    queue<tlm::tlm_generic_payload*>  mp_end_req_pending[NR_IO_OP];


    tlm_utils::peq_with_cb_and_phase<L2Memory> m_peq;
    unsigned int m_bus_width_in_bytes;
    unsigned int m_interleave_depth[NR_IO_OP];
    //boost::numeric::ublas::map_array<sc_dt::uint64, unsigned char> m_storage;
	//int m_storage[];
	map<sc_dt::uint64, unsigned char> m_storage;

//    list<tlm::tlm_generic_payload* > m_txn_in_flight;
    ofstream* mp_fout;
};

#endif

