
#define SC_INCLUDE_DYNAMIC_PROCESSES

#include "tlm_utils/simple_initiator_socket.h"
#include "tlm_utils/simple_target_socket.h"
#include "tlm_utils/multi_passthrough_initiator_socket.h"
#include "tlm_utils/multi_passthrough_target_socket.h"
#include "tlm_utils/peq_with_cb_and_phase.h"
#include <queue>
#include <list>
#include <stdlib.h>

#include "mem.h"
extern ofstream fout;

using namespace sc_core;
using namespace std;
using namespace tlm_utils;

#define DEBUG

//#define PRIORITY

//static ofstream fout("foo.txt");

// ************************************************************************************
// Bus model supports multiple initiators and multiple targets
// Supports b_ and nb_ transport interfaces, DMI and debug
// It does no arbitration, but routes all transactions from initiators without blocking
// It uses a simple built-in routing algorithm
// ************************************************************************************

template<unsigned int BUSWIDTH, unsigned int INITIATOR_NR, unsigned int TARGET_NR>
class XBus: public sc_core::sc_module
{
  // ***********************************************************
  // Each multi-socket can be bound to multiple sockets
  // No need for an array-of-sockets
  // ***********************************************************
	
public:
  SC_HAS_PROCESS(XBus);
  tlm_utils::simple_target_socket_tagged<XBus, BUSWIDTH>    targ_socket[INITIATOR_NR];
  tlm_utils::simple_initiator_socket_tagged<XBus, BUSWIDTH> init_socket[TARGET_NR];

  //SC_CTOR(Bus)
  //: targ_socket("targ_socket"), init_socket("init_socket")
  XBus(sc_module_name module_name, int addr_map)
	  :sc_core::sc_module(module_name)
	  ,NoC_addr_map(addr_map)
  {
	  for(unsigned int i=0; i<INITIATOR_NR; i++)
	  {
		targ_socket[i].register_nb_transport_fw(   this, &XBus::nb_transport_fw, i);
		targ_socket[i].register_b_transport(       this, &XBus::b_transport, i);
		targ_socket[i].register_get_direct_mem_ptr(this, &XBus::get_direct_mem_ptr, i);
		targ_socket[i].register_transport_dbg(     this, &XBus::transport_dbg, i);
	  }
	  for(unsigned int i=0; i<TARGET_NR; i++)
	  {
		init_socket[i].register_nb_transport_bw(          this, &XBus::nb_transport_bw, i);
		init_socket[i].register_invalidate_direct_mem_ptr(this, &XBus::invalidate_direct_mem_ptr, i);
	  }

	  SC_THREAD(thread_nb_write);
	  SC_THREAD(thread_nb_read);
	  //initialize();
  }

#ifdef PRIORITY
  void set_initiator_priority(unsigned int init_nr, unsigned int priority)
  {
	  //m_priority[]
  }
#endif

  /*
  void initialize()
  {
	//set address mapping
    sc_dt::uint64 start_addr;
	sc_dt::uint64 end_addr;
	//FILE * fp = fopen("mem_port_map.cfg", "r");


    
  }
  */

  void thread_nb_write()
  {
	  while(1)
	  {

		  wait(SC_ZERO_TIME);
		  /*sc_dt::uint64 delta_cycle = 0;
		  if(!sc_pending_activity_at_current_time())
			  delta_cycle = sc_delta_count();*/ // COMEMNTME

		  for(unsigned int i=0; i<TARGET_NR; i++)
		  {
			  if(write_trans[i].size()>0)
			  {
			    typename std::list<trans_in_flight *>::iterator it;
		            for(it=write_trans[i].begin();it!=write_trans[i].end();it++)
				{
					if((*it)->in_flight==false)
					{
						//send out the transaction
						// tlm::tlm_sync_enum status = init_socket[i]->nb_transport_fw(*((*it)->Ptrans), (*it)->phase, (*it)->delay);   // COMMENTME
						(*it)->in_flight=true;
						break;
					}
				} 
			  }
		  }
		  wait(1, SC_NS);
	  }
  }
  void thread_nb_read()
  {
	  while(1)
	  {
		  wait(SC_ZERO_TIME);
		  /* sc_dt::uint64 delta_cycle = 0;
		  if(!sc_pending_activity_at_current_time())
			  delta_cycle = sc_delta_count(); */ // COMMENTME

		  for(unsigned int i=0; i<TARGET_NR; i++)
		  {
			  if(read_trans[i].size()>0)
			  {
			    typename std::list<trans_in_flight *>::iterator it;
		            for(it=read_trans[i].begin();it!=read_trans[i].end();it++)
				{
					if((*it)->in_flight==false)
					{
						//send out the transaction
						// tlm::tlm_sync_enum status = init_socket[i]->nb_transport_fw(*((*it)->Ptrans), (*it)->phase, (*it)->delay);   // COMMENTME
						(*it)->in_flight=true;
						break;
					}
				} 
			  }
		  }
		  wait(1, SC_NS);
	  }
  }



  // Tagged non-blocking transport forward method
  // ##############################################
  virtual tlm::tlm_sync_enum nb_transport_fw(int id,
      tlm::tlm_generic_payload& trans, tlm::tlm_phase& phase, sc_time& delay)
  {
    //assert (id < targ_socket.size());
	//  cout<<"In AXI: FW"<<phase<<std::endl;
    // Forward path
    m_id_map[ &trans ] = id;

    sc_dt::uint64 address = trans.get_address();
    sc_dt::uint64 masked_address;
    
    unsigned int target_nr = decode_address( address, masked_address);
	

    if (target_nr < TARGET_NR)
    {
      // Modify address within transaction
      trans.set_address( masked_address );

	  
      // Forward transaction to appropriate target
	  if(phase==tlm::END_RESP)
	  {
        //look up the target NR
           
	    int target_out =  m_id_map_out[ &trans ];

            tranIdExtension* p_mysrcid = trans.get_extension< tranIdExtension>();
	    fout<<"Tran "<<p_mysrcid->m_tran_id<<" : Fun: Dequeue "<<target_out<<" in Module: "<<this->name()<<" Time: "<<sc_time_stamp()<<endl;

	    remove_trans(trans, phase, delay, target_out);
		tlm::tlm_sync_enum status = init_socket[target_out]->nb_transport_fw(trans, phase, delay);
		return status;
	  }
	  else
	  {
		  //tlm::BEGINE_REQ
		  m_id_map_out[ &trans ] = target_nr;

		  tranIdExtension* p_mysrcid = trans.get_extension< tranIdExtension>();
	      fout<<"Tran "<<p_mysrcid->m_tran_id<<" : Fun: Enqueue "<<target_nr<<" in Module: "<<this->name()<<" Time: "<<sc_time_stamp()<<endl;
		  
		  insert_trans(trans, phase, delay, id, target_nr);
		  phase = tlm::END_REQ;
		  return tlm::TLM_UPDATED;
	  }
      //if (status == tlm::TLM_COMPLETED)
      // Put back original address
      //  trans.set_address( address );
      //return status;
    }
    else
      return tlm::TLM_COMPLETED;
  }

  // Tagged non-blocking transport backward method
  // ##############################################
  virtual tlm::tlm_sync_enum nb_transport_bw(int id,
      tlm::tlm_generic_payload& trans, tlm::tlm_phase& phase, sc_time& delay)
  {
    //assert (id < init_socket.size());

    // Backward path

    // Replace original address
	//  cout<<"In AXI: BW"<<phase<<std::endl;
    sc_dt::uint64 address = trans.get_address();
    trans.set_address( compose_address( id, address ) );
	if(phase == tlm::END_REQ)
		return tlm::TLM_ACCEPTED;
    return targ_socket[ m_id_map[ &trans ] ]->nb_transport_bw(trans, phase, delay);
  }

  // Tagged TLM-2 blocking transport method
  virtual void b_transport( int id, tlm::tlm_generic_payload& trans, sc_time& delay )
  {
    //assert (id < targ_socket.size());

    // Forward path
    sc_dt::uint64 address = trans.get_address();
    sc_dt::uint64 masked_address;
    unsigned int target_nr = decode_address( address, masked_address);

    if (target_nr < INITIATOR_NR)
    {
      // Modify address within transaction
      trans.set_address( masked_address );

      // Forward transaction to appropriate target
      init_socket[target_nr]->b_transport(trans, delay);

      // Replace original address
      trans.set_address( address );
    }
  }

  // Tagged TLM-2 forward DMI method
  virtual bool get_direct_mem_ptr(int id,
                                  tlm::tlm_generic_payload& trans,
                                  tlm::tlm_dmi&  dmi_data)
  {
    sc_dt::uint64 masked_address;
    unsigned int target_nr = decode_address( trans.get_address(), masked_address );
    if (target_nr >= INITIATOR_NR)
      return false;

    trans.set_address( masked_address );

    bool status = init_socket[target_nr]->get_direct_mem_ptr( trans, dmi_data );

    // Calculate DMI address of target in system address space
    dmi_data.set_start_address( compose_address( target_nr, dmi_data.get_start_address() ));
    dmi_data.set_end_address  ( compose_address( target_nr, dmi_data.get_end_address() ));

    return status;
  }


  // Tagged debug transaction method
  virtual unsigned int transport_dbg(int id, tlm::tlm_generic_payload& trans)
  {
    sc_dt::uint64 masked_address;
    unsigned int target_nr = decode_address( trans.get_address(), masked_address );
    if (target_nr >= TARGET_NR)
      return 0;
    trans.set_address( masked_address );

    // Forward debug transaction to appropriate target
    return init_socket[target_nr]->transport_dbg( trans );
  }


  // Tagged backward DMI method
  virtual void invalidate_direct_mem_ptr(int id,
                                         sc_dt::uint64 start_range,
                                         sc_dt::uint64 end_range)
  {
    // Reconstruct address range in system memory map
    sc_dt::uint64 bw_start_range = compose_address( id, start_range );
    sc_dt::uint64 bw_end_range   = compose_address( id, end_range );

    // Propagate call backward to all initiators
    for (unsigned int i = 0; i < TARGET_NR; i++)
      targ_socket[i]->invalidate_direct_mem_ptr(bw_start_range, bw_end_range);
  }

  // Simple fixed address decoding
  // In this example, for clarity, the address is passed through unmodified to the target
  inline unsigned int decode_address( sc_dt::uint64 address, sc_dt::uint64& masked_address )
  {
	  masked_address = address;
	  if(NoC_addr_map==0)
	  {
		return rand()%TARGET_NR;
	  }
	  else
	  {
		  //cout<<"address is "<<address<<endl;
		  //cout<<"output target: "<<((address&(0x030)))<<endl;
		  //return address>>(64-NoC_addr_map);
          return 3;
		  //--return address%4;
	  }
		  
/*
	for(int i=0; i<m_addr_map.size(); i++)
	{
		if((address<=m_addr_map[i].second)&&(address>=m_addr_map[i].first))
		{
			masked_address = address;
			return i;
		}
	}
	std::cerr<<"Errorneous address for decoding"<<std::endl;
	return -1;
*/
    //unsigned int target_nr = static_cast<unsigned int>( address & 0x3 );
    //masked_address = address;
    //return target_nr;
  }

  inline sc_dt::uint64 compose_address( unsigned int target_nr, sc_dt::uint64 address)
  {
    return address;
  }


  void remove_trans(tlm::tlm_generic_payload & trans, tlm::tlm_phase & phase, sc_time & delay, unsigned int target_nr)
  {
	bool flag=false;
	if(trans.is_write())
	{
		typename std::list<trans_in_flight *>::iterator it;
		for(it=write_trans[target_nr].begin();it!=write_trans[target_nr].end();it++)
		{
			if((*it)->Ptrans == &trans)
			{
				delete (*it);
				write_trans[target_nr].erase(it);
				flag=true;
				break;
			}
		}
		if(flag==false)
			std::cerr<<"Fatal error, not able to find the write transaction"<<std::endl;
	}
	else if(trans.is_read())
	{
		typename std::list<trans_in_flight *>::iterator it;
		for(it=read_trans[target_nr].begin();it!=read_trans[target_nr].end();it++)
		{
			if((*it)->Ptrans == &trans)
			{
				delete (*it);
				read_trans[target_nr].erase(it);
				flag=true;
				break;
			}
		}
		if(flag==false)
			std::cerr<<"Fatal error, not able to find the read transaction"<<std::endl;
	}
  }
  void insert_trans(tlm::tlm_generic_payload & trans, tlm::tlm_phase & phase, sc_time & delay, unsigned int init_nr, unsigned int target_nr)
  {
	trans_in_flight * cur_trans=new trans_in_flight;
	cur_trans->Ptrans= &trans;
	cur_trans->phase = phase;
	cur_trans->delay = delay;

#ifdef PRIORITY
	cur_trans->in_flight =  false;
	cur_trans->priority=initiator_priority[init_nr];
#endif

	//Push into FIFO
	if(trans.is_write())
	{

#ifdef PRIORITY
		if((write_trans[target_nr].size()==0) || ((write_trans[target_nr].size()==1)&&((write_trans[target_nr].front())->in_flight==true)))
		{
			write_trans[target_nr].push_back(cur_trans);
			return;
		}
		else
		{

			typename std::list<trans_in_flight *>::iterator it;
			for(it=write_trans[target_nr].begin();it!=write_trans[target_nr].end();it++)
			{
				if(((*it)->in_flight==false)&&(((*it)->priority) > (cur_trans->priority)))
				{
					write_trans[target_nr].insert(it,cur_trans);
					break;
				}
			}
			if(it==write_trans[target_nr].end())
				write_trans[target_nr].push_back(cur_trans);
		}
#else
		//NO priority arbitration, according to First In First Out Manner.
		write_trans[target_nr].push_back(cur_trans);
#endif

	}
	else if(trans.is_read())
	{
#ifdef PRIORITY
		if((read_trans[target_nr].size()==0) || ((read_trans[target_nr].size()==1)&&((read_trans[target_nr].front())->in_flight==true)))
		{
			read_trans[target_nr].push_back(cur_trans);
			return;
		}
		else
		{

			typename std::list<trans_in_flight *>::iterator it;
			for(it=read_trans[target_nr].begin();it!=read_trans[target_nr].end();it++)
			{
				if(((*it)->in_flight==false)&&(((*it)->priority) > (cur_trans->priority)))
				{
					read_trans[target_nr].insert(it,cur_trans);
					break;
				}
			}
			if(it==read_trans[target_nr].end())
				read_trans[target_nr].push_back(cur_trans);
		}
#else
		//NO priority arbitration, according to First In First Out Manner.
		read_trans[target_nr].push_back(cur_trans);
#endif
	}

  }

  struct trans_in_flight
  {
	  trans_in_flight():Ptrans(NULL), phase(tlm::TLM_ACCEPTED), delay(SC_ZERO_TIME), 
#ifdef PRIORITY		  
	  priority(88888), 
#endif
	  in_flight(false)
	  {
	  }
	  tlm::tlm_generic_payload * Ptrans;
	  tlm::tlm_phase phase;
	  sc_time delay;
	  bool in_flight;
#ifdef PRIORITY
	  unsigned int priority;
#endif
	  
  };


  std::map <tlm::tlm_generic_payload*, unsigned int> m_id_map;
  std::map <tlm::tlm_generic_payload*, unsigned int> m_id_map_out;
  std::vector<pair<sc_dt::uint64, sc_dt::uint64> > m_addr_map;

  std::map <sc_dt::uint64, unsigned int> m_target;

  std::list<trans_in_flight *> read_trans[TARGET_NR];
  std::list<trans_in_flight *> write_trans[TARGET_NR];

  int NoC_addr_map;

#ifdef PRIORITY
  int initiator_priority[INITIATOR_NR];
#endif 

};



