/*****************************************************************************

  The following code is derived, directly or indirectly, from the SystemC
  source code Copyright (c) 1996-2008 by all Contributors.
  All Rights reserved.

  The contents of this file are subject to the restrictions and limitations
  set forth in the SystemC Open Source License Version 3.0 (the "License");
  You may not use this file except in compliance with such restrictions and
  limitations. You may obtain instructions on how to receive a copy of the
  License at http://www.systemc.org/. Software distributed by Contributors
  under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF
  ANY KIND, either express or implied. See the License for the specific
  language governing rights and limitations under the License.

 *****************************************************************************/

#ifndef __SIMPLE_INITIATOR_SOCKET_DB_H__
#define __SIMPLE_INITIATOR_SOCKET_DB_H__

#include "tlm.h"
#include <sstream>
#include "tlm_utils/simple_initiator_socket.h"
#include "mysql4esl.h"
#include <string>

extern ofstream fout;
/*
ofstream fout_transID1("tranID1.txt");
ofstream fout_transID2("tranID2.txt");
ofstream fout_transID3("tranID3.txt");
ofstream fout_transID4("tranID4.txt");
ofstream fout_transID5("tranID5.txt");
*/

namespace tlm_utils {

template <typename MODULE,
          unsigned int BUSWIDTH = 32,
          typename TYPES = tlm::tlm_base_protocol_types>
class simple_initiator_socket_db :
	public simple_initiator_socket<MODULE, BUSWIDTH, TYPES>
{

  mysql_esl * glb_mysql_esl;
  std::string module_name;
public:
  simple_initiator_socket_db() : simple_initiator_socket<MODULE, BUSWIDTH, TYPES>()  {}

  explicit simple_initiator_socket_db(const char* n) :
    simple_initiator_socket<MODULE, BUSWIDTH, TYPES>(n)
  {
  }

  void register_nb_transport_bw(MODULE* mod, tlm::tlm_sync_enum (MODULE::*cb)(transaction_type&,
                                                             tlm::tlm_base_protocol_types::tlm_phase_type&,
                                                             sc_core::sc_time&))
  {
	module_name=mod->name();
	(*((simple_initiator_socket<MODULE, BUSWIDTH, TYPES> *)this)).register_nb_transport_bw(mod, cb);
  }

  void register_invalidate_direct_mem_ptr(MODULE* mod,
                                          void (MODULE::*cb)(sc_dt::uint64, sc_dt::uint64))
  {
    (*((simple_initiator_socket<MODULE, BUSWIDTH, TYPES> *)this))->register_nb_transport_bw(mod, cb);
  }

  virtual tlm::tlm_sync_enum nb_transport_fw(transaction_type& trans,
                                        tlm::tlm_base_protocol_types::tlm_phase_type& phase,
                                        sc_core::sc_time& t)
  {
#ifdef ESL_SQL
	  (glb_mysql_esl->global_mysql_esl())->esl_tran_pass_skt_insert(trans, (unsigned long)this, phase, 0);
	  tranIdExtension* p_mysrcid = trans.get_extension< tranIdExtension>();
	  //fout<<"Tran "<<p_mysrcid->m_tran_id<<" : Fun: fw_call with Phase "<<phase<<" Module: "<<module_name<<" Time: "<<sc_time_stamp()<<endl;
	  //fout<<"Tran "<<p_mysrcid->m_tran_id<<" : Fun: fw_call with Phase "<<phase<<" at "<<this->name()<<" Time: "<<sc_time_stamp()<<endl;
	  fout<<"Tran "<<p_mysrcid->m_tran_id<<" : Fun: fw_call with Phase "<<phase<<" CMD "<<trans.get_command()<<" at "<<this->name()<<" Time: "<<sc_time_stamp()<<endl;
/*
	  if((phase==tlm::BEGIN_REQ) && (module_name.find("top.init_0")!=-1))
	    fout_transID1<<"Tran "<<p_mysrcid->m_tran_id<<" Created by "<<module_name<<endl;
	  if((phase==tlm::BEGIN_REQ) && (module_name.find("top.init_1")!=-1))
	    fout_transID2<<"Tran "<<p_mysrcid->m_tran_id<<" Created by "<<module_name<<endl;
	  if((phase==tlm::BEGIN_REQ) && (module_name.find("top.init_2")!=-1))
	    fout_transID3<<"Tran "<<p_mysrcid->m_tran_id<<" Created by "<<module_name<<endl;
	  if((phase==tlm::BEGIN_REQ) && (module_name.find("top.init_3")!=-1))
	    fout_transID4<<"Tran "<<p_mysrcid->m_tran_id<<" Created by "<<module_name<<endl;
	  if((phase==tlm::BEGIN_REQ) && (module_name.find("top.init_4")!=-1))
	    fout_transID5<<"Tran "<<p_mysrcid->m_tran_id<<" Created by "<<module_name<<endl;
*/
#endif
	return (*((simple_initiator_socket<MODULE, BUSWIDTH, TYPES> *)this))->nb_transport_fw(trans, phase, t);
  }

  simple_initiator_socket_db * operator ->() 
  {
	  return this;
  }
};
}
#endif
