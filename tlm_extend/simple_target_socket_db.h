
#ifndef __SIMPLE_TARGET_SOCKET_DB_H__
#define __SIMPLE_TARGET_SOCKET_DB_H__

#include "tlm.h"
#include <sstream>
#include "tlm_utils/simple_target_socket.h"
#include "mysql4esl.h"

extern ofstream fout;

namespace tlm_utils {

template <typename MODULE,
          unsigned int BUSWIDTH = 32,
          typename TYPES = tlm::tlm_base_protocol_types>
class simple_target_socket_db :
	public simple_target_socket<MODULE, BUSWIDTH, TYPES>
{
  mysql_esl * glb_mysql_esl;
  std::string module_name;
public:
  simple_target_socket_db() :
    simple_target_socket<MODULE, BUSWIDTH, TYPES>()
  {
  }

  explicit simple_target_socket_db(const char* n) :
    simple_target_socket<MODULE, BUSWIDTH, TYPES>(n)
  {
  }

  // bw transport must come thru us.
  simple_target_socket_db * operator ->() 
  {
	  return this;
  }


  // REGISTER_XXX
  void register_nb_transport_fw(MODULE* mod,  tlm::tlm_sync_enum (MODULE::*cb)(transaction_type&,
                                                             tlm::tlm_base_protocol_types::tlm_phase_type&,
                                                             sc_core::sc_time&))
  {
	  module_name=mod->name();
    (*((simple_target_socket<MODULE, BUSWIDTH, TYPES> *)this)).register_nb_transport_fw(mod, cb);
  }

  void register_b_transport(MODULE* mod,
                            void (MODULE::*cb)(transaction_type&,
                                               sc_core::sc_time&))
  {
    (*((simple_target_socket<MODULE, BUSWIDTH, TYPES> *)this)).register_b_transport(mod, cb);
  }

  virtual tlm::tlm_sync_enum nb_transport_bw(transaction_type& trans,
                                        tlm::tlm_base_protocol_types::tlm_phase_type& phase,
                                        sc_core::sc_time& t)
  {

#ifdef ESL_SQL
	  tranIdExtension* p_mysrcid = trans.get_extension< tranIdExtension>();
	  //fout<<"Tran "<<p_mysrcid->m_tran_id<<" : Fun: bw_call with Phase "<<phase<<" Module: "<<module_name<<" Time: "<<sc_time_stamp()<<endl;
	  fout<<"Tran "<<p_mysrcid->m_tran_id<<" : Fun: bw_call with Phase "<<phase<<" CMD "<<trans.get_command()<<" at "<<this->name()<<" Time: "<<sc_time_stamp()<<endl;
	  (glb_mysql_esl->global_mysql_esl())->esl_tran_pass_skt_insert(trans, (unsigned long)this, phase, 1);
#endif

	//return (*((simple_target_socket<MODULE, BUSWIDTH, TYPES> *)this))->nb_transport_bw(trans, phase, TYPES);
	return (*((simple_target_socket<MODULE, BUSWIDTH, TYPES> *)this))->nb_transport_bw(trans, phase, t);
  }

};

}

#endif
