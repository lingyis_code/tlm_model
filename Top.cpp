
// Filename: tlm2_getting_started_6.cpp

//----------------------------------------------------------------------
//  Copyright (c) 2007-2008 by Doulos Ltd.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//----------------------------------------------------------------------

// Version 1, 26-June-2008
// Version 2,  7-July-2008  Remove N_INITIATORS, N_TARGETS template parameters from Bus
// Version 3   8-March-2010 Replaced target end_req_pending pointer with a queue

// Getting Started with TLM-2.0, Example 6

// Shows the use of multi-sockets in an interconnect component,
// that is, multi_passthrough_initiator_socket and multi_passthrough_target_socket

// This example combines the AT initiator and target from example 4 with the bus from
// example 5, modified to use multi-sockets instead of tagged sockets
// Uses the forward and backward non-blocking transport interfaces of the bus interconnect


#include "AXI_ESL.h"
#include "initiator.h"
#include "target.h"
#include "tlm.h"
#include "systemc.h"
#include "fw_block.h"
#include "ESL_sql_def.h"

#include "i2memory.h"

// *****************************************************************************************
// Top-level module instantiates 4 initiators, a bus, and 4 targets
// *****************************************************************************************

SC_MODULE(Top)
{
  
  XBus<32, 3, 2> *       bus0;
  XBus<32, 2, 2> *       bus1;
  XBus<32, 4, 2> *       bus2;
  XBus<32, 2, 4> *       bus3;

  FW_block<32> *         bus_fw_module[6];
  Initiator* init[5];
  Target*    target[3];

  L2Memory<32> *   l2memory;
  //L2Memory<32> *   l2memory_1;

  SC_CTOR(Top)
  {
        bus0   = new XBus<32, 3, 2>("bus0", 0);
	bus1   = new XBus<32, 2, 2>("bus1", 0);
	bus2   = new XBus<32, 4, 2>("bus2", 0);
	bus3   = new XBus<32, 2, 4>("bus3", 1);

        l2memory = new L2Memory<32>("l2memory");

	for (int i = 0; i < 6; i++)
	{
		char txt[20];
                sprintf(txt, "fw_block_%d", i);
		bus_fw_module[i] = new FW_block<32>(txt);
	}
    // ***************************************************************************
    // bus->init_socket and bus->targ_socket are multi-sockets, each bound 4 times
    // ***************************************************************************

    for (int i = 0; i < 5; i++)
    { 
      char txt[20];
      sprintf(txt, "init_%d", i);
      init[i] = new Initiator(txt);
      if(i<3)
          init[i]->socket.bind( bus0->targ_socket[i] );
      else
    	  init[i]->socket.bind( bus1->targ_socket[i-3] );

    }

    for (int i = 0; i <3; i++)
    {
      char txt[20];
      sprintf(txt, "target_%d", i);
      target[i] = new Target(txt);
      bus3->init_socket[i].bind( target[i]->socket );
    }
	bus3->init_socket[3].bind(l2memory->socket);

	bus0->init_socket[0].bind(bus_fw_module[0]->targ_socket);
	bus0->init_socket[1].bind(bus_fw_module[1]->targ_socket);
	bus1->init_socket[0].bind(bus_fw_module[2]->targ_socket);
	bus1->init_socket[1].bind(bus_fw_module[3]->targ_socket);
	bus2->init_socket[0].bind(bus_fw_module[4]->targ_socket);
	bus2->init_socket[1].bind(bus_fw_module[5]->targ_socket);

	bus_fw_module[0]->init_socket.bind(bus2->targ_socket[0]);
	bus_fw_module[1]->init_socket.bind(bus2->targ_socket[1]);
	bus_fw_module[2]->init_socket.bind(bus2->targ_socket[2]);
	bus_fw_module[3]->init_socket.bind(bus2->targ_socket[3]);
	bus_fw_module[4]->init_socket.bind(bus3->targ_socket[0]);
	bus_fw_module[5]->init_socket.bind(bus3->targ_socket[1]);
  }
};



int sc_main( int argc, char* argv[])
{
    Top top("top");
#ifdef ESL_SQL
	mysql_esl my_mysql_esl;
#endif
    sc_start(5000, SC_NS);
    char c;
	cin>>c;
    return 0;
}
