include Makefile.config

PROJECT	:= Top
SRCS	:= $(wildcard *.cpp)
OBJS	:= $(SRCS:.cpp=.o)

include Makefile.rules
